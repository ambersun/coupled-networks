class netOptions:
	def __init__(self):
		# set options for an element network
	
		# tpye: 'main' or 'gate'. 'main' for the main RNN, 'gate' for 
		# gate network.
	
		self.type = 'main'

		# n_hidden_layer: main network must have 1 hidden layer, gate
		# network can have either 0 or 1 hidden layer.
		
		self.n_hidden_layer = 1

		# n_hidden_units: number of hidden units

		self.n_hidden_units = 100

		# loss_function: the loss function for calculating error 
		
		# self.loss_function = 'cross_entropy'
		self.loss_function = 'cross_entropy'

		# input_dimension: the dimension of an instance of input
		# sequence, including bias (not including previous states or cell values)
		
		self.input_dimension = 200

		# output_dimension: the dimension of output vector
		
		self.output_dimension = 10000

		# extra_input_dimension: the input dimensionf from other parts of the net
		# will be initialized automatically 
		
		self.extra_input_dimension = 100

		# squashing functions can be 'sigmoid', 'tanh', 'softmax'
		# gate_squash: squashing function for gates

		self.gate_squash = 'sigmoid'

		# cell_input_squash: squashing function for cell input

		self.cell_input_squash = 'tanh'

		# cell_output_squash: squashing function for cell output
		
		self.cell_output_squash = 'tanh'

		# output_squash: squashing function for the output layer

		self.output_squash = 'softmax'

		# input_gate: if input gate is on or off

		self.input_gate = 'off'

		# output_gate: if output gate is on or off

		self.output_gate = 'off'

		# forget_gate: if forget gate is on or off

		self.forget_gate = 'off'

		# value should be 'on' or 'off'
		#
		# input_network: the gate network used as input gates

		self.input_network = []

		# output_network: the gate network used as output gates

		self.output_network = []

		# forget_network: the gate network used as forget gates

		self.forget_network = []

		# value should be an ElNet instance

		# learning rate
		self.learning_rate = 0.01

		# momentum
		self.momentum = 0.9

		# name used to save the network
		self.save_name = 'coupledNN'
		
		# GPU: 'on' or 'off'. Selecting 'on' will run the
		# implementation on GPU

		self.GPU = 'off'
