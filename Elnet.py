import numpy as np
from netOptions import netOptions
import netUtil

class Elnet:

	def __init__(self):
		
		# options
        	self.opts = netOptions()
		 
		if self.opts.type == 'main':
			Elnet.main_net = self     	

	def init_parameters(self):

		np.random.seed(15213)
		self.input_value = np.zeros([self.opts.input_dimension,1])
        	self.output_value = np.zeros([self.opts.output_dimension,1])
		if self.opts.type == 'main':
			self.word_entropy = 0
			self.perplexity = 0
		else:
			self.gradient = np.zeros([self.opts.output_dimension,1])

		# initializes gate networks
		if self.opts.input_gate	== 'on':
			self.opts.input_network.opts.extra_input_dimension = self.opts.n_hidden_units
			self.opts.input_network.opts.output_dimension = self.opts.n_hidden_units
			self.opts.input_network.init_parameters()
		if self.opts.output_gate == 'on':
			self.opts.output_network.opts.extra_input_dimension = self.opts.n_hidden_units
			self.opts.output_network.opts.output_dimension = self.opts.n_hidden_units
			self.opts.output_network.init_parameters()
		if self.opts.forget_gate == 'on':
			self.opts.forget_network.opts.extra_input_dimension = self.opts.n_hidden_units
			self.opts.forget_network.opts.output_dimension = self.opts.n_hidden_units
			self.opts.forget_network.init_parameters()	

		if self.opts.n_hidden_layer != 0:
			# initialize attributes
			self.input_gates_value = np.ones([self.opts.n_hidden_units,1])
			self.output_gates_value = np.ones([self.opts.n_hidden_units,1])
			self.forget_gates_value = np.zeros([self.opts.n_hidden_units,1])
			
			self.cell_value = np.zeros([self.opts.n_hidden_units,1])
			self.cell_output_value = np.zeros([self.opts.n_hidden_units,1])

			self.cell_cache = [np.zeros([self.opts.n_hidden_units,1]).T]
			self.cell_output_cache = [np.zeros([self.opts.n_hidden_units,1]).T]

			# weights
			# One hidden layer network
			# input -> cell
			self.w_ic = np.random.uniform(-0.1,0.1,[self.opts.n_hidden_units,self.opts.input_dimension])
			self.d_w_ic = np.zeros(np.shape(self.w_ic))
			# previous cell output -> cell
			self.w_hc = np.random.uniform(-0.1,0.1,[self.opts.n_hidden_units,1])
			self.d_w_hc = np.zeros(np.shape(self.w_hc))
			# cell -> output
			self.w_co = np.random.uniform(-0.1,0.1,[self.opts.output_dimension,self.opts.n_hidden_units])
			self.d_w_co = np.zeros(np.shape(self.w_co))
		else:
			# No hidden layer network
			# input -> output
			self.w_io = np.random.uniform(-0.1,0.1,[self.opts.output_dimension,self.opts.input_dimension])
			self.d_w_io = np.zeros(np.shape(self.w_io))
			# previous cell output -> output 
			self.w_ho = np.random.uniform(-0.1,0.1,[self.opts.output_dimension,self.opts.extra_input_dimension])
			self.d_w_ho = np.zeros(np.shape(self.w_ho))
			# cell -> output
			self.w_co = np.random.uniform(-0.1,0.1,[self.opts.output_dimension,self.opts.extra_input_dimension])
			self.d_w_co = np.zeros(np.shape(self.w_co))	

	@staticmethod
	def train_net(main_net,train_data,valid_data,dictionary):

		netUtil.create_log('begin',main_net.opts.save_name,[],[],[])
		best_valid_ppl = 99999

		np.random.seed(15213)
		train_length = len(train_data)
		valid_length = len(valid_data)		
		train_word_cnt = 0
		train_sum_entropy = 0

		for epoch in xrange(1,11): # iterate epoch
			print 'epoch ' + str(epoch) + ':'
			print 'training ...'
			ind = np.random.permutation(train_length)			
			u = 0
			for i in ind: # iterate sentence
				for j in xrange(len(train_data[i])-1): # iterate words
					if train_data[i][j] in dictionary.keys():
						vec = dictionary[train_data[i][j]]
					else:
						vec = dictionary['<unk>']
					# add a bias				
					vec = np.insert(vec,0,[1.0]) # convert 1d array to 2d
					input_vec = np.reshape(vec,[main_net.opts.input_dimension,1])
				
					# entire net forward propagation
					if main_net.opts.input_gate == 'on':
						main_net.opts.input_network.input_value = input_vec
						main_net.opts.input_network.forward_pass([],j)
						main_net.input_gates_value = main_net.opts.input_network.output_value
					if main_net.opts.output_gate == 'on':
						main_net.opts.output_network.input_value = input_vec
						main_net.opts.output_network.forward_pass([],j)
						main_net.output_gates_value = main_net.opts.output_network.output_value
					if main_net.opts.forget_gate == 'on':
						main_net.opts.forget_network.input_value = input_vec
						main_net.opts.forget_network.forward_pass([],j)
						main_net.forget_gates_value = main_net.opts.forget_network.output_value
									
					main_net.input_value = input_vec
					y = netUtil.one_hot(train_data[i][j+1],sorted(dictionary.keys()))
	
					main_net.forward_pass(y,j)
					train_word_cnt += 1
					train_sum_entropy += main_net.word_entropy
					main_net.perplexity = np.power(2,train_sum_entropy/train_word_cnt)

					# entire net backpropagation

					main_net.backward_pass(y)					
			
					if main_net.opts.input_gate == 'on':
						main_net.opts.input_network.backward_pass([])
					if main_net.opts.output_gate == 'on':
						main_net.opts.output_network.backward_pass([])
					if main_net.opts.forget_gate == 'on':
						main_net.opts.forget_network.backward_pass([])	
				
				print 'Perplexity: '+str(main_net.perplexity)


			# run validation after every epoch

			netUtil.create_log('record_ppl',main_net.opts.save_name,epoch,main_net.perplexity,'')

			print 'validation ...'
			netUtil.create_log('record_msg',main_net.opts.save_name,[],[],'validation ...')

			if main_net.perplexity < best_valid_ppl:
				best_valid_ppl = main_net.perplexity
				netUtil.save(main_net)

			Elnet.test_net(main_net,valid_data,dictionary)

	@staticmethod
	def test_net(main_net,test_data,dictionary):
		
		test_word_cnt = 0
		test_sum_entropy = 0

		for sentence in test_data: # iterate sentence
			for i in xrange(len(sentence)-1): # iterate words

				if sentence[i] in dictionary.keys():
					vec = dictionary[sentence[i]]
				else:
					vec = dictionary['<unk>']
				# add a bias
				
				vec = np.insert(vec,0,[1.0])
				input_vec = np.reshape(vec,[main_net.opts.input_dimension,1])

				if main_net.opts.input_gate == 'on':
					main_net.opts.input_network.input_value = input_vec
					main_net.opts.input_network.forward_pass([],i)
					main_net.input_gates_value = main_net.opts.input_network.output_value
				if main_net.opts.output_gate == 'on':
					main_net.opts.output_network.input_value = input_vec
					main_net.opts.output_network.forward_pass([],i)
					main_net.output_gates_value = main_net.opts.output_network.output_value
				if main_net.opts.forget_gate == 'on':
					main_net.opts.forget_network.input_value = input_vec
					main_net.opts.forget_network.forward_pass([],i)
					main_net.forget_gates_value = main_net.opts.forget_network.output_value

				main_net.input_value = input_vec
				y = netUtil.one_hot(sentence[i+1],sorted(dictionary.keys()))
				main_net.forward_pass(y,i)

				test_word_cnt += 1
				test_sum_entropy += main_net.word_entropy
				main_net.perplexity = np.power(2,test_sum_entropy/test_word_cnt)
			print 'Perplexity: '+str(main_net.perplexity)	
		
		netUtil.create_log('record_ppl',main_net.opts.save_name,[],main_net.perplexity,'')

	def forward_pass(self,y,ind):
		
		if self.opts.n_hidden_layer == 1:

			if ind == 0: # begin of a sentence, clear all states and caches
				self.cell_value = np.zeros([self.opts.n_hidden_units,1])
				self.cell_output_value = np.zeros([self.opts.n_hidden_units,1])
				self.cell_cache = [np.zeros([self.opts.n_hidden_units,1]).T]
				self.cell_output_cache = [np.zeros([self.opts.n_hidden_units,1]).T]
  
    			ig = self.input_gates_value;
    			og = self.output_gates_value;
    			fg = self.forget_gates_value;
	    		# cell value at t-1
	    		s_pc = self.cell_cache[-1].T;

	    		x = self.input_value;
	    		# cell output at t-1
	    		b_h = self.cell_output_cache[-1].T;	    		

	    		a_c = np.dot(self.w_ic,x) + self.w_hc * b_h;
			
	    		s_c = fg * s_pc + ig * netUtil.squash(a_c,self.opts.cell_input_squash,'forward');
	    
	    		b_c = og * netUtil.squash(s_c,self.opts.cell_output_squash,'forward');
	    
	    		self.cell_value = s_c;
	    		self.cell_output_value = b_c;

			if self.opts.type == 'main':
	    			self.output_value = netUtil.squash(np.dot(self.w_co,b_c), self.opts.output_squash,'forward');
				self.word_entropy = -np.log2(self.output_value[np.where(y==1)])
			else:
				self.output_value = b_c
				
			# store cell values, and cell output values		
			self.cell_cache.append(self.cell_value.T)
			self.cell_output_cache.append(self.cell_output_value.T)
    
		elif self.opts.type == 'gate':
			
			x = self.input_value;

			b_h = Elnet.main_net.cell_output_cache[-1].T;
			s_pc = Elnet.main_net.cell_cache[-1].T;

		    	a = np.dot(self.w_io,x) + np.dot(self.w_ho,b_h) + np.dot(self.w_co,s_pc);

		    	self.output_value = netUtil.squash(a,self.opts.gate_squash,'forward');
			
	
	def backward_pass(self, y):

		# return
		if self.opts.n_hidden_layer == 1:
			if self.opts.type == 'main':
				# compute derivatives:
				
				if self.opts.output_squash == 'softmax' and self.opts.loss_function == 'cross_entropy':
					# checkpoint: d_err_d_a_w, ndict * 1
					d_err_d_a_w = self.output_value - y
				else:
					# d_err_d_w_pred ndict * 1
					d_err_d_w_pred = netUtil.error(y, self.output_value, self.opts.loss_function, 'backward')
					# d_w_pred/d_a_w ndict * ndict	
						
					d_w_pred_d_a_w = netUtil.squash(np.dot(self.w_co,self.cell_output_value), self.opts.output_squash, 'backward')

					# checkpoint: d_err_d_a_w, ndict * 1
					d_err_d_a_w = np.dot(d_w_pred_d_a_w.T,d_err_d_w_pred)


				# d_a_w/d_w_co = b_c, should be ndict * nhidden * ndict (use equivalent form instead)
				d_a_w_d_w_co = self.cell_output_cache[-1]


				# d_a_w/d_b_c = w_co, ndict * nhidden
				d_a_w_d_b_c = self.w_co

				# checkpoint: d_err_d_b_c, ndict * 1
				d_err_d_b_c = np.dot(d_a_w_d_b_c.T,d_err_d_a_w)

			else:
				d_err_d_b_c = self.gradient
			
			if self.opts.output_gate == 'on':
				# d_b_c/d_og = diag(h_s_c), nhidden * nhidden, # output gate
				d_b_c_d_og = netUtil.diag(netUtil.squash(self.cell_cache[-1].T,self.opts.cell_output_squash,'forward'))
				self.opts.output_network.gradient = np.dot(d_b_c_d_og.T,d_err_d_b_c)

			# d_b_c/d_h_s_c = diag(og), nhidden * nhidden
			d_b_c_d_h_s_c = netUtil.diag(self.output_gates_value)
			# d_h_s_c/d_s_c, nhidden * nhidden
			d_h_s_c_d_s_c = netUtil.squash(self.cell_cache[-1].T,self.opts.cell_output_squash,'backward')		

			# checkpoint: d_err_d_s_c, n_hidden * 1
			d_err_d_s_c = np.dot(np.dot(d_h_s_c_d_s_c.T,d_b_c_d_h_s_c.T),d_err_d_b_c)

			a_c = np.dot(self.w_ic,self.input_value) + self.w_hc * self.cell_output_cache[-2].T

			if self.opts.forget_gate == 'on':
				# d_s_c/d_fg = diag(s_c_t-1), nhidden * nhidden, # forget gate
				d_s_c_d_fg = netUtil.diag(self.cell_cache[-2].T)
				self.opts.forget_network.gradient = np.dot(d_s_c_d_fg.T,d_err_d_s_c)

			if self.opts.input_gate == 'on':
				# d_s_c/d_ig = diag(g(a_c)), nhidden * nhidden, # input gate
				d_s_c_d_ig = netUtil.diag(netUtil.squash(a_c,self.opts.cell_input_squash,'forward'))
				self.opts.input_network.gradient = np.dot(d_s_c_d_ig.T,d_err_d_s_c)

			# d_s_c/d_g_a_c = diag(ig), nhidden * nhidden
			d_s_c_d_g_a_c = netUtil.diag(self.input_gates_value)
			# d_g_a_c/d_a_c, nhidden * nhidden
			
			d_g_a_c_d_a_c = netUtil.squash(a_c,self.opts.cell_input_squash,'backward')
			# checkpoint: d_err_d_a_c, nhidden * 1
			d_err_d_a_c = np.dot(np.dot(d_g_a_c_d_a_c.T,d_s_c_d_g_a_c.T),d_err_d_s_c)		
			

			# d_a_c/d_w_hc = b_h_t-1, should be nhidden * nhidden * nhidden (use equivalent form instead)
			d_a_c_d_w_hc = self.cell_output_cache[-2]
			# d_a_c/d_w_ic = x.T, should be nhidden * ninput * nhidden (use equivalent form instead)
			d_a_c_d_w_ic = self.input_value.T			
			
			# update
			self.d_w_hc = self.opts.learning_rate * netUtil.diag(np.dot(d_err_d_a_c,d_a_c_d_w_hc)) + self.opts.momentum * self.d_w_hc	
			self.w_hc = self.w_hc - self.d_w_hc
			
			self.d_w_ic = self.opts.learning_rate * np.dot(d_err_d_a_c,d_a_c_d_w_ic) + self.opts.momentum * self.d_w_ic		
			self.w_ic = self.w_ic - self.d_w_ic
			
			if self.opts.type == 'main':
				self.d_w_co = self.opts.learning_rate * np.dot(d_err_d_a_w,d_a_w_d_w_co) + self.opts.momentum * self.d_w_co		
				self.w_co = self.w_co - self.d_w_co

		elif self.opts.type == 'gate': # 0 hidden layer
	
			x = self.input_value;

			b_h = Elnet.main_net.cell_output_cache[-2].T;
			s_pc = Elnet.main_net.cell_cache[-2].T;

		    	a = np.dot(self.w_io,x) + np.dot(self.w_ho,b_h) + np.dot(self.w_co,s_pc);
			
			# d_grad/d_a, 
			d_grad_d_a = netUtil.squash(a,self.opts.gate_squash,'backward')		

			# d_a/d_w_io = x, should be noutput * ninput * noutput (use equivalent form instead)
			d_a_d_w_io = x.T
			# d_a/d_w_ho = b_h.T, should be noutput * nextra-input * noutput (use equivalent form instead)
			d_a_d_w_ho = b_h.T
			# d_a/d_w_co = s_pc.T, should be noutput * nextra-input * noutput (use equivalent form instead)
			d_a_d_w_co = s_pc.T
	
			# update
			self.d_w_io = self.opts.learning_rate * np.dot(np.dot(d_grad_d_a.T,self.gradient),d_a_d_w_io) + self.opts.momentum * self.d_w_io
			self.w_io = self.w_io - self.d_w_io
			self.d_w_ho = self.opts.learning_rate * np.dot(np.dot(d_grad_d_a.T,self.gradient),d_a_d_w_ho) + self.opts.momentum * self.d_w_ho
			self.w_ho = self.w_ho - self.d_w_ho
			self.d_w_co = self.opts.learning_rate * np.dot(np.dot(d_grad_d_a.T,self.gradient),d_a_d_w_co) + self.opts.momentum * self.d_w_co
			self.w_co = self.w_co - self.d_w_co
