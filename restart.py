import pickle
import sys
from Elnet import Elnet
import netUtil

if __name__ == '__main__':

	fname = sys.argv[-1]
	print 'restarting '+fname+'...'
	# load data
	dictionary = pickle.load(open('./data/ptb/dictionary.pickle','r'))
	train_data = []
	valid_data = []
	test_data = []

	with open('./data/ptb/ptb.train.txt') as f:
		data = f.readlines()
		for line in data:
			train_data.append(line.rstrip().split())
	
	with open('./data/ptb/ptb.valid.txt') as f:
		data = f.readlines()
		for line in data:
			valid_data.append(line.rstrip().split())

	with open('./data/ptb/ptb.test.txt') as f:
		data = f.readlines()
		for line in data:
			test_data.append(line.rstrip().split())


	main_net = pickle.load(open('./save/' + fname +'.pickle','r'))
	Elnet.main_net = main_net

	Elnet.train_net(main_net,train_data,valid_data,dictionary)
	print 'testing...'
	Elnet.test_net(main_net,test_data,dictionary)




