from Elnet import Elnet
import pickle
	
# load data
dictionary = pickle.load(open('./data/ptb/dictionary.pickle','r'))
train_data = []
valid_data = []
test_data = []

with open('./data/ptb/ptb.train.txt') as f:
	data = f.readlines()
	for line in data:
		train_data.append(line.rstrip().split())
	
with open('./data/ptb/ptb.valid.txt') as f:
	data = f.readlines()
	for line in data:
		valid_data.append(line.rstrip().split())

with open('./data/ptb/ptb.test.txt') as f:
	data = f.readlines()
	for line in data:
		test_data.append(line.rstrip().split())

ig_net = Elnet()
ig_net.opts.type = 'gate'
ig_net.opts.n_hidden_layer = 1

og_net = Elnet()
og_net.opts.type = 'gate'
og_net.opts.n_hidden_layer = 1

fg_net = Elnet()
fg_net.opts.type = 'gate'
fg_net.opts.n_hidden_layer = 1

main_net = Elnet()
main_net.opts.input_gate = 'on'
main_net.opts.input_network = ig_net
main_net.opts.output_gate = 'on'
main_net.opts.output_network = og_net
main_net.opts.forget_gate = 'on'
main_net.opts.forget_network = fg_net

main_net.opts.save_name = 'coupled_lstm'

main_net.init_parameters()

Elnet.train_net(main_net,train_data,valid_data,dictionary)
print 'testing...'
Elnet.test_net(main_net,test_data,dictionary)
