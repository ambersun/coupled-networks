# check gradient
from Elnet import Elnet
import numpy as np
import netUtil
from copy import deepcopy as cp

# check main net

def fake_forward(m,y):
	
	x = m.input_value;

	b_h = m.cell_output_cache[-2].T;
	s_pc = m.cell_cache[-2].T;	

	if m.opts.input_gate is 'on':
		g = m.opts.input_network
		a_g = g.w_io.dot(x) + g.w_ho.dot(b_h) + g.w_co.dot(s_pc)
		m.input_gates_value = netUtil.squash(a_g, 'sigmoid','forward')
	if m.opts.output_gate is 'on':
		g = m.opts.output_network
		a_g = g.w_io.dot(x) + g.w_ho.dot(b_h) + g.w_co.dot(s_pc)
		m.output_gates_value = netUtil.squash(a_g, 'sigmoid','forward')
	if m.opts.forget_gate is 'on':
		g = m.opts.forget_network
		a_g = g.w_io.dot(x) + g.w_ho.dot(b_h) + g.w_co.dot(s_pc)
		m.forget_gates_value = netUtil.squash(a_g, 'sigmoid','forward')


	a = m.w_ic.dot(m.input_value) + m.w_hc * m.cell_output_cache[-2].T
	c = m.forget_gates_value * m.cell_cache[-2].T + m.input_gates_value * np.tanh(a)
	b = m.output_gates_value * np.tanh(c)
	a_w = m.w_co.dot(b)
	output = netUtil.squash(a_w,'softmax','forward')
	error = netUtil.error(y,output,'cross_entropy','forward')
	
	return error

def grad_check(m,y,var):
	eps = 0.000000001
	####
	m.opts.forget_network.w_co[1,0] += eps
	e1 = fake_forward(m,y)
	m.opts.forget_network.w_co[1,0] -= 2*eps
	e2 = fake_forward(m,y)
	####	
	print 'Gradient difference:'
	print var
	print (e1-e2)/2/eps
	print np.linalg.norm(var-(e1-e2)/2/eps)

g = Elnet() # gate net
g.opts.input_dimension = 3
g.opts.n_hidden_layer = 0
g.opts.learning_rate = 1
g.opts.momentum = 0
g.opts.type = 'gate'

m = Elnet() # main net
m.opts.input_dimension = 3
m.opts.output_dimension = 3
m.opts.n_hidden_units = 2
m.opts.learning_rate = 1
m.opts.momentum = 0

####
m.opts.forget_network = g
m.opts.forget_gate = 'on'
####

m.init_parameters()
m.input_value = np.reshape(np.array([1,2,3]),[3,1])
g.input_value = m.input_value
m.cell_cache = [np.reshape(np.array([0,0]),[1,2]),np.reshape(np.array([1,2]),[1,2])]
m.cell_output_cache = [np.reshape(np.array([0,0]),[1,2]),np.tanh(np.reshape(np.array([1,2]),[1,2]))]

y = np.reshape(np.array([0,1,0]),[3,1])
g.forward_pass(y,1)

####
m.forget_gates_value = g.output_value
####

m.forward_pass(y,1)
u = cp(m)

m.backward_pass(y)
g.backward_pass(y)
var = g.d_w_co

grad_check(u,y,var[1,0])
