import numpy as np
import time
import pickle

def squash(arr_in, func, direction):
	if direction == 'forward':
		if func == 'sigmoid':
			return 1.0/(1.0+np.exp(-arr_in))

		elif func == 'softmax':
			c = np.max(arr_in)
			arr = np.exp(arr_in-c)
			return arr/np.sum(arr)		

		elif func == 'tanh':
			return np.tanh(arr_in)
	
	elif direction == 'backward':  
		if func == 'sigmoid':
			s = 1.0/(1.0+np.exp(-arr_in))
			return diag(s*(1.0-s))

		elif func == 'softmax':
			arr = np.exp(arr_in)
			arr = arr/np.sum(arr)
			return diag(arr)-np.dot(arr,arr.T)		

		elif func == 'tanh':
			return diag(1.0-np.square(np.tanh(arr_in)))

def error(y, arr, func,direction):
	if direction == 'forward':
		if func == 'l2':
			return 0.5 * np.square(np.linalg.norm(y-arr))
		elif func == 'KL':
			ind = np.where(y == 1)
			if arr[ind] == 0:
				return 0.0
			else:
				return log(1.0/arr[ind])
		elif func == 'cosine':
			return np.dot(y.T,arr)/np.linalg.norm(y)/np.linalg.norm(arr)

		elif func == 'cross_entropy':
			return -np.sum(y*np.log(arr))			

	elif direction == 'backward': # calculate the gradient
		if func == 'l2':
			return y-arr

		elif func == 'KL':
			pass
		elif func == 'cosine':
			pass
		elif func == 'cross_entropy':
			return -y/arr
			

def one_hot(word,keys):
	vec = np.zeros([len(keys),1])
	if word in keys:
		vec[keys.index(word)] = 1;
	else:
		vec[keys.index('<unk>')] = 1;
	return vec

def diag(mat):
	# input should be np.array format
	if mat.ndim == 2:
		shape = np.shape(mat)
		if shape[0] == shape[1]:
			return np.reshape(np.diag(mat),[shape[0],1])
		elif min(shape) == 1:
			mat = np.reshape(mat,[max(shape),])
	return np.diag(mat)	

def create_log(phase,fname,nEpoch,ppl,msg):
	if phase == 'begin':
		with open('./log/'+fname+'.txt','w+') as f:
			f.write('Begin: ')
			f.write(time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.localtime()))
			f.write('\n\n')
			
	elif phase == 'record_ppl':
		with open('./log/'+fname+'.txt','a') as f:
			f.write('Epoch '+str(nEpoch)+': ')
			f.write(time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.localtime()))
			f.write('\n')
			f.write('Perplexity: '+str(ppl)+'\n\n')

	elif phase == 'record_msg':
		with open('./log/'+fname+'.txt','a') as f:
			f.write(time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.localtime()))
			f.write('\n')
			f.write(msg+'\n\n')

	elif phase == 'end':
		with open('./log/'+fname+'.txt','a') as f:
			f.write('End: ')
			f.write(time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.localtime()))
			f.write('\n\n')
		
def save(net):
	fname = './save/' + net.opts.save_name + '.pickle'
	pickle.dump(net,open(fname,'w+'))

def load(fname):
	return pickle.load(open(fname))

