from Elnet import Elnet
import pickle
	
# load data
dictionary = pickle.load(open('./data/ptb/dictionary.pickle','r'))
train_data = []
valid_data = []
test_data = []

with open('./data/ptb/ptb.train.txt') as f:
	data = f.readlines()
	for line in data:
		train_data.append(line.rstrip().split())
	
with open('./data/ptb/ptb.valid.txt') as f:
	data = f.readlines()
	for line in data:
		valid_data.append(line.rstrip().split())

with open('./data/ptb/ptb.test.txt') as f:
	data = f.readlines()
	for line in data:
		test_data.append(line.rstrip().split())

ig_net = Elnet()
ig_net.opts.type = 'gate'
ig_net.opts.n_hidden_layer = 0

main_net = Elnet()
main_net.opts.input_gate = 'on'
main_net.opts.input_network = ig_net

main_net.opts.save_name = 'input'

main_net.init_parameters()

Elnet.train_net(main_net,train_data,valid_data,dictionary)
print 'testing...'
Elnet.test_net(main_net,test_data,dictionary)
