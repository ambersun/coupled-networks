from Elnet import Elnet
from disp_net import disp
import pickle
	
# load data
dictionary = pickle.load(open('./data/small_example/dictionary.pickle','r'))
train_data = []

with open('./data/small_example/train.txt') as f:
	data = f.readlines()
	for line in data:
		train_data.append(line.rstrip().split())

# ig_net = Elnet()
# ig_net.opts.type = 'gate'
# ig_net.opts.n_hidden_layer = 0
# ig_net.opts.input_dimension = 3


main_net = Elnet()
main_net.opts.input_dimension = 3
main_net.opts.output_dimension = 5
main_net.opts.n_hidden_units = 2
# main_net.opts.input_gate = 'on'
# main_net.opts.input_network = ig_net
main_net.init_parameters()

# disp(ig_net)
disp(main_net)
Elnet.train_net(main_net,train_data,train_data,dictionary)
# disp(ig_net)
disp(main_net)
print 'testing...'
